#!/bin/bash
USERNAME=jesuslopez
HOSTS="loki.ist.unomaha.edu"
SCRIPT="cd public_html;cd group7-john-test ;ls;git pull origin master; publish-public-html"
for HOSTNAME in ${HOSTS} ; do
    ssh -l ${USERNAME} ${HOSTNAME} "${SCRIPT}"
done